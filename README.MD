# Node-red on windows with microbit

https://nodered.org/docs/getting-started/windows

* Install Node
* npm install -g --unsafe-perm node-red
* node-red

# Palettes

https://flows.nodered.org/node/node-red-node-serialport
https://flows.nodered.org/node/node-red-dashboard


# Docker

Tried first with docker on windows... but it failed

docker run -it -p 1880:1880 --name node-red --privileged --user=root nodered/node-red

Fik hul igennem til en af tty enhederne... men ingen data.

# Docker compose

Not tested the compose file yet.

# Raspberry Pi

Should be extended to an IoT hub.

https://nodered.org/docs/getting-started/raspberrypi
https://github.com/gcgarner/IOTstack/issues/80

# Inspiration from

https://www.youtube.com/watch?v=0ywbzN9ZKa0
